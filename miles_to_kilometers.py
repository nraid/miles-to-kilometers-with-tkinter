import tkinter

window = tkinter.Tk()
window.title("Mile to Km Converter")
window.minsize(width=200, height=100)
window.config(padx=20, pady=20)

# Empty label
empty = tkinter.Label()
empty.grid(column=0, row=0)


# Input for Km
input_km = tkinter.Entry(width=7)
input_km.grid(column=2, row=0)


# Label Miles
label_miles = tkinter.Label(text='Miles')
label_miles.grid(column=3, row=0)


# Label is equal to
is_equal = tkinter.Label(text='is equal to')
is_equal.grid(column=0, row=1)


# Label with kilometers int
km = tkinter.Label(text=0)
km.grid(column=2, row=1)


# Label Km
label_miles = tkinter.Label(text='Km')
label_miles.grid(column=3, row=1)


def calculate():
    km_calc = round(int(input_km.get()) * 1.60934)
    km.config(text=km_calc)


# Button
button = tkinter.Button(text='Calculate', command=calculate)
button.grid(column=2, row=2)










window.mainloop()